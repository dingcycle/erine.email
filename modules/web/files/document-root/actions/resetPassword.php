<?php
include("../core/config.php");

if(count($_POST))
{
  $input = new \ptejada\uFlex\Collection($_POST);
  $user->resetPassword($input->mailAddress);
  $res = $user->resetPassword($input->mailAddress);
  if ($res)
  {
    $subject = "Choose a new password";
    $message  = "Dear erine.email user,\n\n";
    $message .= "Looks like you lost your password. To get a new one, please click this link or paste the address into your browser:\n";
    $message .= "https://erine.email/updatePassword?c=" . $res->confirmation . "\n\n";
    $message .= "See you there in a moment,\n\n";
    $message .= "Your erine.email robot";
    $header = "From:erine.email robot <robot@erine.email> \r\n";
    $retval = mail($input->mailAddress, $subject, $message, $header);
    if( $retval == false )
    {
      $user->log->formError('mailAddress', 'Failed sending confirmation email');
    }
  }
  echo json_encode(
    array(
      'error'   => $user->log->getErrors(),
      'confirm' => 'All good!',
      'form'    => $user->log->getFormErrors(),
    )
  );
}
?>
