<?php
include('../core/config.php');

if(count($_POST))
{
    /*
     * Covert POST into a Collection object
     * for better value handling
     */
    $input = new \ptejada\uFlex\Collection($_POST);

	$user->login($input->username, $input->password, $input->auto);

	$errMsg = '';

	if($user->log->hasError()){
		$errMsg = $user->log->getErrors();
		$errMsg = $errMsg[0];
	}

	echo json_encode(array(
		'error'    => $user->log->getErrors(),
		'confirm'  => "You are now logged-in as $user->username",
		'form'     => $user->log->getFormErrors(),
	));
}
?>
