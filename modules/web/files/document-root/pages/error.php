<div class="content-parent">
  <div class="content-child-middle">
    <div class="container marketing">
      <i class="fa fa-exclamation fa-message" aria-hidden="true"></i>
      <h2>Oh nooo!</h2>
      <p>Looks like something wrong happened.<br>
      Unexpected? <a href='&#109;&#097;&#105;&#108;to:&#101;&#114;&#114;&#111;&#114;&#064;erine.email'>Send me an email</a> and tell me how you got there.</p>
      <p><a class="btn btn-viewdetails" href="/" role="button">Home &raquo;</a></p>
    </div>
  </div>
</div>
