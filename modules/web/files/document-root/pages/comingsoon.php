<div class="content-parent">
  <div class="content-child-middle">
    <div class="container marketing">
      <i class="fa fa-clock-o fa-message" aria-hidden="true"></i>
      <h2>Coming soon</h2>
      <p>erine.email is still in development. Come back later!</p>
      <p><a class="btn btn-viewdetails" href="/" role="button">Home &raquo;</a></p>
    </div>
  </div>
</div>
