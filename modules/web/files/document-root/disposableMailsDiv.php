<?php
  // This script is called by pages/disposableMails.php. It sets the content of the infoDiv div.
  include('core/config.php');
  if (!$user->isSigned())
  {
    exit();
  }
  $stmt = sqlquery($pdo, 'SELECT count(*) as "da", sum(sent) as "st", sum(dropped) as "dr" FROM disposableMailAddress WHERE userID = :id', ['id' => $user->ID]);
  $row = $stmt->fetch();
  if ($row['da'] > 0)
  {
    echo $row['st'] . " email" . (($row['st'] > 1) ? "s" : "") . " sent, " . $row['dr'] . " dropped.<br>";
  }
  switch ($row['da'])
  {
    case 0:  $stmt = sqlquery($pdo, 'SELECT username FROM user WHERE ID = :id', ['id' => $user->ID]);
             $row = $stmt->fetch();
             echo "You have no disposable address yet. Send an email to <i>something</i>." . $row['username'] . "@erine.email to start the adventure!";
             break;
    case 1:  echo "You have 1 disposable address.<br>";
             $stmt = sqlquery($pdo, 'SELECT enabled FROM disposableMailAddress WHERE userID = :id', ['id' => $user->ID]);
             $row = $stmt->fetch();
             echo "This address is ";
             echo ($row['enabled'] == 1) ? "enabled and will forward you emails." : "disabled.";
             break;
    default: echo "You have " . $row['da'] . " disposable addresses.<br>";
             $stmt = sqlquery($pdo, 'SELECT count(*) as "en" FROM disposableMailAddress WHERE userID = :id and enabled = 1', ['id' => $user->ID]);
             $row = $stmt->fetch();
             switch ($row['en'])
             {
               case 0:  echo "None of them are enabled. That means only new disposable email addresses will forward you emails.";
                        break;
               case 1:  echo "1 of them is enabled and will forward you emails.";
                        break;
               default: echo $row['en'] . " of them are enabled and will forward you emails.";
                        break;
             }
             break;
  }
?>
