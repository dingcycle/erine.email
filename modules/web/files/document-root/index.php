<?php

include('core/config.php');

// Set $pageInclude from URL
$base = dirname($_SERVER['PHP_SELF']);
if ($base == "/")
{
  $base = "";
}
$pagePath = substr($_SERVER['REQUEST_URI'], strlen($base) + 1);
$pagePath = explode('?', $pagePath);
$pagePath = $pagePath[0];
$pagePath = trim($pagePath,"/");
if (!$pagePath)
{
  $pagePath = 'home';
}
$pageInclude = "pages/$pagePath.php";

// Page not found
if(!file_exists($pageInclude) || strpos($pageInclude, ".."))
{
  $pageInclude = "pages/404.php";
}

include 'header.php';
include 'menu.php';
include $pageInclude;
include 'foot.html';
?>
