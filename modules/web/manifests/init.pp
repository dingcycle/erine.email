# Install the whole website
class web
{

  package { [
    'apache2',
    'php',
    'php-mysql',
  ]:
    ensure  => present,
    # We need to change the www-data homedir before running Apache HTTPd with
    # it. If we don't, usermod will fail with a "user www-data is currently
    # used by process <httpd_pid>" error
    require => User['www-data'],
  }

  file { '/etc/apache2':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    recurse => true,
    ignore  => [ 'mods-available', 'mods-available/*', 'sites-available/*', 'sites-enabled/*' ],
    force   => true,
    purge   => true,
    source  => 'puppet:///modules/web/apache2',
    notify  => Exec['reloadApache2'],
    # We need to overwrite the /etc/apache2/ directory, installed by the apache2
    # package, and we need Let's encrypt certificate to be installed before
    # deploying this conf as Certbot needs a running Apache HTTPd with the
    # default configuration, provided by the package
    require => [
      Package['apache2'],
      Class['letsencrypt'],
    ],
  }

  file { '/etc/apache2/sites-available':
    ensure  => directory,
    recurse => true,
    purge   => true,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    # We need the /etc/apache2 requirements
    require => File['/etc/apache2'],
  }

  file { '/etc/apache2/sites-enabled':
    ensure  => directory,
    recurse => true,
    purge   => true,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    # We need the /etc/apache2 requirements
    require => File['/etc/apache2'],
  }

  $domain = hiera('domainnames')
  web::install_site { $domain : }

  file { '/var/www':
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    recurse => true,
    force   => true,
    purge   => true,
    source  => 'puppet:///modules/web/document-root',
  }

  # Do NOT add a requirement on the above /etc/apache2/ as Certbot needs a
  # running Apache HTTPd with the default configuration, provided by the
  # package, and as a service reload will be triggered while updating the conf
  service { 'apache2':
    ensure  => 'running',
    require => Package['apache2'],
  }

  exec { 'reloadApache2':
    command     => '/bin/systemctl reload apache2.service',
    refreshonly => true,
    require     => Package['apache2'],
  }

  file { '/etc/php/7.3/cli/php.ini':
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('web/php.ini.cli.erb'),
    notify  => Exec['reloadApache2'],
    # We will overwrite the file provided by php7.3-cli package (part of the php meta package)
    require => Package['php'],
  }

  file { '/etc/php/7.3/apache2/php.ini':
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('web/php.ini.apache2.erb'),
    notify  => Exec['reloadApache2'],
    # We will overwrite the file provided by libapache2-mod-php7.3 package (part of the php meta package)
    require => Package['php'],
  }

}
